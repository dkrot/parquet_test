#!/usr/bin/env bash

set -e
set -o pipefail

##
## performance test of several scenarios:
## - write parquet file
## - read parquet file (all, linearly)
## - write ORC file
## - read ORC file (all, linearly)
##

stage() {
    echo "-----------------------------------------------------------"
    echo "STAGE: $*"
}

THISDIR=$( readlink -f $( dirname $0 ) )
BINDIR=$THISDIR/build/install/parquet_test/bin
NRECORDS=${NRECORDS:-3000000} # requires ~ 2.5Gb RAM in Java
DATADIR=${DATADIR:-/tmp}      # usually, /tmp is OK

stage "Prepare, generating $NRECORDS records"
$THISDIR/../tnfp/sslkeys_generator/sslkeys_generator -m $NRECORDS >$DATADIR/sslkeys.txt

stage "Write Parquet"; $BINDIR/write_parquet.sh $DATADIR/sslkeys.par <$DATADIR/sslkeys.txt 2>/dev/null
stage "Write ORC"; $BINDIR/write_orc.sh $DATADIR/sslkeys.orc <$DATADIR/sslkeys.txt 2>/dev/null

stage "Read Parquet"; $BINDIR/read_parquet_linear.sh $DATADIR/sslkeys.par 2>/dev/null
stage "Read ORC"; $BINDIR/read_orc_linear.sh $DATADIR/sslkeys.orc 2>/dev/null
