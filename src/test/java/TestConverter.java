import static org.junit.Assert.*;
import org.junit.Test;



public class TestConverter {
    @Test
    public void testHex2Bytes() {
        assertArrayEquals(new byte[]{(byte)0x9A, 0x53, (byte)0xF8, 0x7F, (byte)0xFF},
                RequestInfo.HexString2Bytes("9A53F87FFF"));
    }
}
