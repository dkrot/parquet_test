class TimeStat {
    static void printStat(long size, long duration_nano) {
        System.out.printf("time: %d ms; size: %d mb; speed: %.1f mbs\n",
                duration_nano / 1000000,
                size >> 20,
                size / (duration_nano / 1000000000.0) / 1024 / 1024);
    }
}
