import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.exec.vector.BytesColumnVector;
import org.apache.hadoop.hive.ql.exec.vector.LongColumnVector;
import org.apache.hadoop.hive.ql.exec.vector.VectorizedRowBatch;
import org.apache.orc.OrcFile;
import org.apache.orc.Reader;
import org.apache.orc.RecordReader;

import java.io.Closeable;
import java.io.IOException;

public class OrcLinearReader implements Closeable {
    Reader reader;
    RecordReader rows;

    OrcLinearReader(Configuration conf, Path path) throws IOException {
        reader = OrcFile.createReader(path, OrcFile.readerOptions(conf));
        rows = reader.rows(); // read everything
    }

    static long readOrc(Configuration conf, Path path) throws IOException {
        long begin = System.nanoTime();
        OrcLinearReader reader = new OrcLinearReader(conf, path);

        reader.ReadFull();
        return System.nanoTime() - begin;
    }

    public static void main(String[] args) throws IOException {
        Path path = new Path(args[0]);
        Configuration conf = new Configuration();
        FileSystem fs = path.getFileSystem(conf);
        long file_len = fs.getFileStatus(path).getLen();

        TimeStat.printStat(file_len, readOrc(conf, path));

        // try one more time to get more precise results
        // due to [lazy] Configuration initializations
        TimeStat.printStat(file_len, readOrc(conf, path));
    }

    private void ReadFull() throws IOException {
        VectorizedRowBatch batch = reader.getSchema().createRowBatch();

        LongColumnVector v_src_ip_hash = (LongColumnVector) batch.cols[0];
        LongColumnVector v_time = (LongColumnVector) batch.cols[1];
        BytesColumnVector v_src_ip = (BytesColumnVector) batch.cols[2];
        LongColumnVector v_src_port = (LongColumnVector)batch.cols[3];
        BytesColumnVector v_dst_ip = (BytesColumnVector) batch.cols[4];
        LongColumnVector v_dst_port = (LongColumnVector)batch.cols[5];
        BytesColumnVector v_tls = (BytesColumnVector) batch.cols[6];
        BytesColumnVector v_payload = (BytesColumnVector) batch.cols[7];
        BytesColumnVector v_session_id = (BytesColumnVector) batch.cols[8];
        BytesColumnVector v_server_random = (BytesColumnVector) batch.cols[9];

        long sum = 0; // just to do something with extracted data

        while (rows.nextBatch(batch)) {
            for (int r = 0; r < batch.size; r++) {
                sum += v_src_ip_hash.vector[r];
            }
        }
    }

    @Override
    public void close() throws IOException {
        rows.close();
    }
}
