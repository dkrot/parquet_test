import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.exec.vector.LongColumnVector;
import org.apache.hadoop.hive.ql.exec.vector.VectorizedRowBatch;
import org.apache.hadoop.hive.ql.io.sarg.PredicateLeaf;
import org.apache.hadoop.hive.ql.io.sarg.SearchArgument;
import org.apache.hadoop.hive.ql.io.sarg.SearchArgumentFactory;
import org.apache.orc.*;

import java.io.IOException;
import java.util.List;

public class OrcDemoReader {
    Path path;

    OrcDemoReader(String path) throws IOException {
        this.path = new Path(path);
    }

    void printStripeStatistics() throws IOException {
        Configuration conf = new Configuration();
        Reader reader = OrcFile.createReader(path, OrcFile.readerOptions(conf));
        List<StripeStatistics> stat =  reader.getStripeStatistics();
        List<StripeInformation> stripes = reader.getStripes();

        int s = 0;
        for (StripeStatistics st: stat) {
            IntegerColumnStatistics col1_stat = ((IntegerColumnStatistics)st.getColumnStatistics()[1]);
            StripeInformation stripe = stripes.get(s);

            System.out.printf("%d. %d..%d [offset: %d; len: %d]\n",
                    s, col1_stat.getMinimum(), col1_stat.getMaximum(),
                    stripe.getOffset(), stripe.getLength());

            s++;
        }
    }

    void print() throws IOException {
        int n = 3;
        long begin = 0;
        Configuration conf = new Configuration();

        for (int i = 0; i <= n; i++) {
            if (i == 1)
                begin = System.nanoTime();

            SearchArgument sarg = SearchArgumentFactory.newBuilder()
                    .equals("src_ip_hash", PredicateLeaf.Type.LONG, 500000000L)
                    .build();

            Reader reader = OrcFile.createReader(path, OrcFile.readerOptions(conf));

            RecordReader rows = reader.rows(reader.options().range(60416838, 60461166).searchArgument(sarg,
                    new String[]{null, "src_ip_hash", "time", "src_ip", "src_port", "dst_port", "tls", "payload"}));
            VectorizedRowBatch batch = reader.getSchema().createRowBatch();
            LongColumnVector src_ip_hash = (LongColumnVector) batch.cols[0];

            while (rows.nextBatch(batch)) {
                for (int r = 0; r < batch.size; r++)
                    System.out.printf("%d\n", src_ip_hash.vector[r]);
            }
        }

        System.out.printf("avg time (excluding first): %d ms\n",
                (System.nanoTime() - begin) / n / 1000000);
    }

    static public void main(String[] args) throws Exception {
        new OrcDemoReader(args[0]).print();
    }
}
