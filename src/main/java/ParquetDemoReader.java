import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.parquet.example.data.Group;
import org.apache.parquet.filter2.compat.FilterCompat;
import org.apache.parquet.filter2.predicate.FilterPredicate;
import org.apache.parquet.hadoop.ParquetReader;
import org.apache.parquet.hadoop.example.GroupReadSupport;

import java.io.Closeable;
import java.io.IOException;

import static org.apache.parquet.filter2.predicate.FilterApi.*;

public class ParquetDemoReader implements Closeable {
    ParquetReader<Group> reader = null;

    public ParquetDemoReader(Configuration conf, Path path) throws IOException {
        GroupReadSupport readSupport = new GroupReadSupport();
        FilterPredicate pred = eq(intColumn("src_ip_hash"), 50000);

        reader = ParquetReader.builder(readSupport, path)
                .withConf(conf)
                .withFilter(FilterCompat.get(pred)).build();
    }

    public RequestInfo Next() throws IOException {
        Group grp = reader.read();
        if (grp == null)
            return null;


        return new RequestInfo(grp.getInteger("src_ip_hash", 0));
    }

    public static void main(String[] args) throws IOException {
        Path path = new Path(args[0]);

        Configuration conf = new Configuration();
        int n = 3;
        long begin = 0;

        for (int i = 0; i <= n; i++) {
            if (i == 1)
                begin = System.nanoTime();

            ParquetDemoReader rd = new ParquetDemoReader(conf, path);

            while (true) {
                RequestInfo rec = rd.Next();
                if (rec == null)
                    break;

                System.out.printf("ip_hash: %d\n", rec.src_ip_hash);
            }

            rd.close();
        }

        System.out.printf("avg time (excluding first): %d ms\n",
                (System.nanoTime() - begin) / n / 1000000);
    }

    @Override
    public void close() throws IOException {
        reader.close();
    }
}
