import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.parquet.column.ParquetProperties;
import org.apache.parquet.example.data.Group;
import org.apache.parquet.example.data.GroupFactory;
import org.apache.parquet.example.data.simple.SimpleGroupFactory;
import org.apache.parquet.hadoop.ParquetWriter;
import org.apache.parquet.hadoop.example.GroupWriteSupport;
import org.apache.parquet.hadoop.metadata.CompressionCodecName;
import org.apache.parquet.schema.MessageType;
import org.apache.parquet.schema.MessageTypeParser;

import java.io.Closeable;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class ParquetDemoWriter implements Closeable {
    private GroupFactory groupFactory = null;
    private ParquetWriter<Group> writer = null;

    private ParquetDemoWriter(Configuration conf, Path path) throws IOException {
        MessageType schema = MessageTypeParser.parseMessageType("message Message {\n" +
                "required int32  src_ip_hash;\n" +
                "required int64  time;\n" +
                "required binary src_ip (UTF8);\n" +
                "required int32  src_port;\n" +
                "required binary dst_ip (UTF8);\n" +
                "required int32  dst_port;\n" +
                "required binary tls;\n" +
                "required binary payload;\n" +
                "required binary session_id;\n" +
                "required binary server_random;\n" +
                "}");
        groupFactory = new SimpleGroupFactory(schema);
        GroupWriteSupport writeSupport = new GroupWriteSupport();
        GroupWriteSupport.setSchema(schema, conf);

        writer = new ParquetWriter<Group>(path,
                writeSupport,
                CompressionCodecName.SNAPPY,
                ParquetWriter.DEFAULT_BLOCK_SIZE,
                ParquetWriter.DEFAULT_PAGE_SIZE,
                ParquetWriter.DEFAULT_PAGE_SIZE,
                true,
                false,
                ParquetProperties.WriterVersion.PARQUET_2_0, conf);
    }

    private Group getGroup() {
        return groupFactory.newGroup();
    }

    private void Append(RequestInfo req) throws IOException {
        Group grp = getGroup()
                .append("src_ip_hash", req.src_ip_hash)
                .append("time", req.time)
                .append("src_ip", req.src.ip)
                .append("src_port", req.src.port)
                .append("dst_ip", req.dst.ip)
                .append("dst_port", req.dst.port)
                .append("tls", req.tls)
                .append("payload", req.payload)
                .append("session_id", req.session_id)
                .append("server_random", req.server_random);
        writer.write(grp);
    }

    private static long writeParquet(Configuration conf, Path path,
                             ArrayList<RequestInfo> requests) throws IOException {
        ParquetDemoWriter demo = new ParquetDemoWriter(conf, path);
        long begin = System.nanoTime();

        for (RequestInfo req: requests) {
            demo.Append(req);
        }
        demo.close();

        return System.nanoTime() - begin;
    }

    public static void main(String[] args) throws IOException {
        Configuration conf = new Configuration();
        Path path = new Path(args[0]);
        FileSystem fs = path.getFileSystem(conf);

        ArrayList<RequestInfo> reqs = new ArrayList<>();
        long input_size = 0;

        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String line = sc.nextLine();
            input_size += line.length();
            try {
                RequestInfo req = new RequestInfo(line);
                reqs.add(req);
            }
            catch(ParseException e) {
                System.err.printf("Exception: \"%s\" on line \"%s\"\n", e.getMessage(), line);
                System.exit(1);
            }
        }

        if (fs.exists(path))
            fs.delete(path, false);

        TimeStat.printStat(input_size, writeParquet(conf, path, reqs));
        fs.delete(path, false);

        // try one more time to get more precise results
        // due to [lazy] Configuration loading (~1 second)
        TimeStat.printStat(input_size, writeParquet(conf, path, reqs));
    }

    @Override
    public void close() throws IOException {
        writer.close();
    }
}

