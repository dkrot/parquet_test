import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.parquet.example.data.Group;
import org.apache.parquet.hadoop.ParquetReader;
import org.apache.parquet.hadoop.example.GroupReadSupport;

import java.io.Closeable;
import java.io.IOException;

public class ParquetLinearReader implements Closeable {
    ParquetReader<Group> reader = null;

    ParquetLinearReader(Configuration conf, Path path) throws IOException {
        GroupReadSupport readSupport = new GroupReadSupport();

        reader = ParquetReader.builder(readSupport, path)
                .withConf(conf).build();
    }

    private static long readParquet(Configuration conf, Path path) throws IOException {
        long begin = System.nanoTime();
        ParquetLinearReader reader = new ParquetLinearReader(conf, path);

        while (true) {
            if (!reader.ReadNext())
                break;
        }

        return System.nanoTime() - begin;
    }

    public static void main(String[] args) throws IOException {
        Path path = new Path(args[0]);
        Configuration conf = new Configuration();
        FileSystem fs = path.getFileSystem(conf);
        long file_len = fs.getFileStatus(path).getLen();

        TimeStat.printStat(file_len, readParquet(conf, path));

        // try one more time to get more precise results
        // due to [lazy] Configuration loading (~1 second)
        TimeStat.printStat(file_len, readParquet(conf, path));
    }

    boolean ReadNext() throws IOException {
        Group grp = reader.read();
        if (grp == null)
            return false;

        grp.getInteger(0, 0);
        grp.getLong(1, 0);
        grp.getBinary(2, 0);
        grp.getInteger(3, 0);
        grp.getBinary(4, 0);
        grp.getInteger(5, 0);
        grp.getBinary(6, 0);
        grp.getBinary(7, 0);
        grp.getBinary(8, 0);
        grp.getBinary(9, 0);

        return true;
    }

    @Override
    public void close() throws IOException {
        reader.close();
    }
}
