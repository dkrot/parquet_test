import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.exec.vector.BytesColumnVector;
import org.apache.hadoop.hive.ql.exec.vector.LongColumnVector;
import org.apache.hadoop.hive.ql.exec.vector.VectorizedRowBatch;
import org.apache.orc.CompressionKind;
import org.apache.orc.OrcFile;
import org.apache.orc.TypeDescription;
import org.apache.orc.Writer;

import java.io.Closeable;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class OrcDemoWriter implements Closeable {
    private Writer writer;
    private VectorizedRowBatch batch;

    OrcDemoWriter(Configuration conf, Path path) throws IOException {
        TypeDescription schema = TypeDescription.createStruct()
                        .addField("src_ip_hash", TypeDescription.createInt())
                        .addField("time", TypeDescription.createInt())
                        .addField("src_ip", TypeDescription.createString())
                        .addField("src_port", TypeDescription.createShort())
                        .addField("dst_ip", TypeDescription.createString())
                        .addField("dst_port", TypeDescription.createShort())
                        .addField("tls", TypeDescription.createBinary())
                        .addField("payload", TypeDescription.createBinary())
                        .addField("session_id", TypeDescription.createBinary())
                        .addField("server_random", TypeDescription.createBinary());

        writer = OrcFile.createWriter(path,
                OrcFile.writerOptions(conf).setSchema(schema).compress(CompressionKind.SNAPPY).rowIndexStride(1000));

        batch = schema.createRowBatch();
    }

    private void Append(List<RequestInfo> requests) throws IOException {
        LongColumnVector src_ip_hash = (LongColumnVector)batch.cols[0];
        LongColumnVector time = (LongColumnVector)batch.cols[1];
        BytesColumnVector src_ip = (BytesColumnVector)batch.cols[2];
        LongColumnVector src_port = (LongColumnVector)batch.cols[3];
        BytesColumnVector dst_ip = (BytesColumnVector)batch.cols[4];
        LongColumnVector dst_port = (LongColumnVector)batch.cols[5];
        BytesColumnVector tls = (BytesColumnVector)batch.cols[6];
        BytesColumnVector payload = (BytesColumnVector)batch.cols[7];
        BytesColumnVector session_id = (BytesColumnVector)batch.cols[8];
        BytesColumnVector server_random = (BytesColumnVector)batch.cols[9];

        for (RequestInfo req: requests) {
            int i = batch.size;

            src_ip_hash.vector[i] = req.src_ip_hash;
            time.vector[i] = req.time;
            src_ip.setVal(i, req.src.ip.getBytes());
            src_port.vector[i] = req.src.port;
            dst_ip.setVal(i, req.dst.ip.getBytes());
            dst_port.vector[i] = req.dst.port;
            tls.setVal(i, req.tls.getBytes());
            payload.setVal(i, req.payload.getBytes());
            session_id.setVal(i, req.session_id.getBytes());
            server_random.setVal(i, req.server_random.getBytes());

            if (++batch.size == batch.getMaxSize()) {
                flush();
            }
        }
    }

    private static long writeOrc(Configuration conf, Path path,
                                     ArrayList<RequestInfo> requests) throws IOException
    {
        OrcDemoWriter demo = new OrcDemoWriter(conf, path);
        long begin = System.nanoTime();
        demo.Append(requests);
        demo.flush();
        demo.close();

        return System.nanoTime() - begin;
    }

    public static void main(String[] args) throws IOException {
        Path path = new Path(args[0]);
        Configuration conf = new Configuration();
        ArrayList<RequestInfo> reqs = new ArrayList<>();
        FileSystem fs = path.getFileSystem(conf);
        long input_size = 0;

        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String line = sc.nextLine();
            input_size += line.length();
            try {
                reqs.add(new RequestInfo(line));
            }
            catch(ParseException e) {
                System.err.printf("Exception: \"%s\" on line \"%s\"\n", e.getMessage(), line);
                System.exit(1);
            }
        }

        if (fs.exists(path))
            fs.delete(path, false);

        TimeStat.printStat(input_size, writeOrc(conf, path, reqs));

        // try one more time to get more precise results
        fs.delete(path, false);
        TimeStat.printStat(input_size, writeOrc(conf, path, reqs));
    }

    @Override
    public void close() throws IOException {
        writer.close();
    }

    private void flush() throws IOException {
        if (batch.size != 0) {
            writer.addRowBatch(batch);
            batch.reset();
        }
    }
}
