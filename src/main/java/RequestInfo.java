/*
 * 2017-04-21T16:11:06+03:00 amigo-nginx1  178.72.71.14:6449 217.69.139.252:443 36103AC31A6E34E5A1A630CE62705FBB1A2DB2CDD381D97B76495AFFE25633FC A60099447A32402DF3C5253E83EA8CA14F78518A7F070CD4208D28DBA08D9A53AED00B583E74A646EBEF27C6D2F8874D
 *
 * 0) [hash]
 * 1) 2017-04-21T16:11:06+03:00
 * 2) amigo-nginx1
 * 3) 178.72.71.14:6449 (from)
 * 4) 217.69.139.252:443 (to)
 * 5) 36103AC31A6E34E5A1A630CE62705FBB1A2DB2CDD381D97B76495AFFE25633FC (tls)
 * 6) A60099447A32402DF3C5253E83EA8CA14F78518A7F070CD4208D28DBA08D9A53A (payload)
 */

import org.apache.parquet.io.api.Binary;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class RequestInfo {
    private static final SimpleDateFormat timestampFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");

    class IpPortPair {
        String ip;
        int port;

        IpPortPair(String s) throws ParseException {
            int pos = s.lastIndexOf(':');
            if (pos == -1) {
                throw new ParseException(String.format("Invalid IP:port: \"%s\"", s), pos);
            }
            ip = s.substring(0, pos);
            port = Integer.valueOf(s.substring(pos+1));
        }
    }

    long time;
    int src_ip_hash;
    IpPortPair src;
    IpPortPair dst;
    Binary tls;
    Binary payload;
    Binary session_id;
    Binary server_random;

    private long parseTime(String s) throws ParseException {
        return Long.valueOf(s);
    }

    static public byte[] HexString2Bytes(String s) {
        byte[] str_bytes = s.getBytes();
        byte[] raw_bytes = new byte[str_bytes.length / 2];

        for (int i = 0; i < raw_bytes.length; i++) {
            int chr = (Character.digit(str_bytes[i*2], 16) << 4) | Character.digit(str_bytes[i*2 + 1], 16);
            raw_bytes[i] = (byte)chr;
        }

        return raw_bytes;
    }

    static public Binary HexString2Binary(String s) {
        return Binary.fromConstantByteArray(HexString2Bytes(s));
    }

    RequestInfo(String line) throws ParseException {
        String[] parts = line.split("\\s+");

        time = parseTime(parts[0]);
        // skip service-name, [1]
        src = new IpPortPair(parts[2]);
        dst = new IpPortPair(parts[3]);
        tls = HexString2Binary(parts[4]);
        payload = HexString2Binary(parts[5]);
        session_id = HexString2Binary(parts[6]);
        server_random = HexString2Binary(parts[7]);

        src_ip_hash = src.ip.hashCode();
    }

    RequestInfo(int src_ip_hash) {
        this.src_ip_hash = src_ip_hash;
    }
}
